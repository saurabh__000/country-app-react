
import CountryDetail from '../../components/country-detail';
import CountryFlag from '../../components/country-flags';
import ErrorBanner from '../../components/error-banner'
import React from 'react';
import ExportButton from '../../components/export-button'

import html2pdf from 'html2pdf.js'
import styles from '../../styles/Detail.module.css'




export default class Post extends React.Component {
  constructor(props) {
    super(props);
    this.state={      
      data:[]
    }
  }
  static async getInitialProps(ctx) {
    const alpha3Code = ctx.query.alpha3Code;
    
    const res = await fetch("https://restcountries.eu/rest/v2/alpha/"+alpha3Code)
     const data = await res.json()
    
  
     if (!data) {
       return {
         notFound: true,
       }
     }
  
    
    return {
      data: data 
    }
  }
   exportToPDF(){
     if(window!==undefined){
      console.log("export to pdf")
      const divToDisplay = document.getElementById('country-detail')
      var opt = {
       margin:       1,
       filename:     'myfile.pdf',
       image:        { type: 'jpeg', quality: 0.98 },
       
       html2canvas :{  dpi: 300,letterRendering: true,useCORS: true},
       jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
     };
     
     // New Promise-based usage:
     
     html2pdf().set(opt).from(divToDisplay).save();
   
     }  
     
   }
   
   
    render(){
      
      return (
        <div className="container-fluid">
          <div className="row justify-content-center">
		      <div className="col-md-9">
			      <div className="container pt-3" id="country-detail">
            <div className="row">
              <div className="col-sm-4 col-md-3">
                <span><h4 className={`${styles.country_name_heading}`}>{this.props.data.name}</h4></span>
              </div>
              <div className="col-sm-8  col-md-9 d-flex ">
              <div>
              <ExportButton onClick={this.exportToPDF} buttonName="Export to PDF"/>l̥
              </div>
              </div>
            </div>
        
              <CountryDetail data={this.props.data}/>
              {
                this.props.data.borders.length==0?<ErrorBanner message="No Neighbouring Countries"/>:<CountryFlag data={this.props.data}/>
              }
              	
	          </div>
		      </div>
	      </div>
        </div>

        
      
      )

    }
    
  }
  

  