import HomePage from './home-page'
import React from 'react'
import axios from 'axios';
export default class Home extends React.Component  {
  
  constructor(props) {
    super(props);
    this.state={      
      data:[]
    }
    this.getApiData();
    //Set initial state
  }
  
   getApiData() {
    axios.get('https://restcountries.eu/rest/v2/all')
    .then( (response)=> {
      // handle success
      
      const apiData = response.data;
      this.setState( {data: apiData});
      
      //this.delta(apiData)

    })
    .catch( (error)=> {
      // handle error
      console.log(error);
    })
    .then(()=> {
      // always executed
    });
    
  
    
  }
  
  render(){
    
    
    return (
      
      <HomePage countries={this.state.data}/> 
     )
  }
  
}
