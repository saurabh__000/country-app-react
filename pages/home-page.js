
import CountryCard from '../components/country-card'
import SearchBar from '../components/searchbar'

import styles from '../styles/Home.module.css'
import React, { useState, useEffect } from "react";
import ExportButton from '../components/export-button'
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
export default class HomePage extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      input: '',
      fullData:[],
      data:[]
    }
        
  }
  UNSAFE_componentWillReceiveProps(nextProps){
    
    this.setState({fullData:nextProps.countries})
  }
   
   updateInput =  async(input) => {
    const filtered = this.props.countries.filter(country => {
     return country.name.toLowerCase().includes(input.toLowerCase())
    })
    console.log(input)
    console.log(this.props)
    console.log(filtered)
    this.setState({input:input})
    this.state.fullData=filtered
    
 }
 exportToExcel=()=>{
   var csvData=[]
   for(var i in this.state.fullData){
    
     csvData.push(
       {
         "id":Number(i)+1,
         "Name":this.state.fullData[i].name,
         "Area":this.state.fullData[i].area,
         "Capital":this.state.fullData[i].capital,
         "Flag URL":this.state.fullData[i].flag,
         "Region":this.state.fullData[i].region
        }
        )
   }
   
   const fileName="Country Excel File"
   const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
   const fileExtension = '.xlsx';

   
       const ws = XLSX.utils.json_to_sheet(csvData);
       const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };
       const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
       const data = new Blob([excelBuffer], {type: fileType});
       FileSaver.saveAs(data, fileName + fileExtension);
  

 }
  renderHome=()=>{
    
  }
  
  render(){
    
    return (    
      <div className="container-fluid">
      <div className="row justify-content-center">
      <div className="col-sm-12 col-md-9 col-lg-7 col-xl-6">
        <div className="container pt-3">
      <section>
          <h2 className={styles.heading_countries}>Countries</h2>	
      </section>
        <SearchBar 
         input={this.state.input} 
         onChange={this.updateInput} />
         <div className="row d-flex justify-content-end mt-2 mb-1">
         <div className="col-md-4 ">
         <ExportButton onClick={this.exportToExcel} buttonName="Export to Excel"/>
         </div>
         </div>
         
         
         
        <section >
          
        {this.state.fullData.map(country=> ( 
          <React.Fragment key={country.name}>
            <CountryCard country={country}/>
          </React.Fragment>

     
                
            ))         
            }
        </section>        
        </div>
        </div>
        </div>
        </div>
    )

  }
  
}

