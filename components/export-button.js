import React from 'react'
import styles from '../styles/Home.module.css'
export default class ExportButton extends React.Component {  
    render(){
        return (
            				
					<button type="button" className={`${styles.show_button } btn btn-outline-primary btn-block`} onClick={() => this.props.onClick() }>{this.props.buttonName}</button>						
				   
            
              
          )
    }
  
}