import styles from '../styles/Home.module.css'
export default function SearchBar({keyword,onChange}) {  
  return (
    <section>
    <div className="row" >
        <div className="input-group col-md-12" >
            <input 
            type="text" 
            className={`${styles.svg_append} search-query form-control`} 
            placeholder="Search countries"  
            value={keyword}
            onChange={(e) => onChange(e.target.value)}
            />  
        </div>
    </div>   
    </section>
      
  )
}