
import React from 'react';
import styles from '../styles/Detail.module.css'
import ExportButton from './export-button'
import { jsPDF } from "jspdf";
import { toPng } from "html-to-image";


export default class CountryDetail extends React.Component {
	 
	constructor(props) {
		super(props);
		this.state={      
		  data:[]
		}
		
	    
	    
	  }
	  getCurrency(){
		var currency="";
		for(var curr in this.props.data.currencies){
			if(curr<1){
				currency+=this.props.data.currencies[curr].name;
			}else{
				currency+=","+this.props.data.currencies[curr].name;
			}
			
		}
		
		return currency;

	  }
	  getLanguages(){
		var language="";
		for(var lang in this.props.data.languages){
			if(lang<1){
				language+=this.props.data.languages[lang].name;
			}else{
				language+=","+this.props.data.languages[lang].name;
			}
			
		}
		return language

	  }
	                             
	  getTimezones(){
		var timezones="";
		for(var tz in this.props.data.timezones){
			if(tz<1){
				timezones+=this.props.data.timezones[tz];
			}else{
				timezones+=","+this.props.data.timezones[tz];
			}
			
		}
		return timezones;
	  }                    	
	  getCallingCodes(){
		  
	    var countryCode="";
		for(var cc in this.props.data.callingCodes){
			if(cc<1){
				countryCode+=this.props.data.callingCodes[cc];
			}else{
				countryCode+=","+this.props.data.callingCodes[cc];
			}
			
		}
		return countryCode;

	  }    
	   exportToPdf=async (html)=>{
		const image = await toPng(html.current,{quality:0.95});
		const doc = new jsPDF();
	
		  doc.addImage(image,'JPEG',5,22,200,160);
		  doc.save();
	
	
	  }  
	                	
	  
	  //console.log(data)
	render(){
		return (
			<section>
					<div   className={`${styles.country_container} `} >
						<div>
						
						
						<div>
                        
    
                        </div>
						</div>
						
						<div className="row">
							<div className="col-md-5">
								<img src={this.props.data.flag} className="img-fluid"/>
							</div>
							<div className="col-md-7 mt-3" >
								<div className={`${styles.country_detail_container} row`}>
									<div className="col-md-12">Capital: {this.props.data.capital}</div>
								</div>
								<div className={`${styles.country_detail_container} row`}>
									<div className="col-md-12" >Population: {this.props.data.population}</div>
									
								</div>
								<div className={`${styles.country_detail_container} row`}>
									<div className="col-md-12" >Region: {this.props.data.region}</div>
									
								</div>
								<div className={`${styles.country_detail_container} row`}>
									<div className="col-md-12" >Sub-region: {this.props.data.subregion}</div>
									
								</div>
								<div className={`${styles.country_detail_container} row`}>
									<div className="col-md-12" >Area: {this.props.data.area}<sup>2</sup></div>
									
								</div>
								<div className={`${styles.country_detail_container} row`}>
									<div className="col-md-12" >Country Code: {this.getCallingCodes()}</div>
									
								</div>
								<div className={`${styles.country_detail_container} row`}>
									<div className="col-md-12" >Language: {this.getLanguages()}</div>
									
								</div>
								<div className={`${styles.country_detail_container} row`}>
									<div className="col-md-12">Currencies: {this.getCurrency()}</div>
								</div>
								<div className={`${styles.country_detail_container} row`}>
									<div className="col-md-12">Timezones: {this.getTimezones()}</div>
									
								</div>
								
								
							</div>
						</div>
					</div>
				</section>      
		  )

	}
  
}