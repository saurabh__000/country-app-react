import React from 'react'
import CountryCard from '../components/country-card'
import styles from '../styles/Detail.module.css'

export default class CountryFlag extends React.Component {
  constructor(props){
    super(props)
  }
  render(){
    return (

      <section>
        <div className={`${styles.country_container} mt-3`} >
        <h4 className={`${styles.country_name_heading}`}>Neighbour Countries</h4>
        <div className={`${styles.neighbour_rows} row`} >
                          {
                           this.props.data.borders.map(borders=>(
                              
                              <div className={`${styles.countries_flag} col-md-4`}>
                              <img src={`https://restcountries.eu/data/${borders.toLowerCase()}.svg`} className="img-fluid"/>
                          </div>
                           ))   
                          
                          }          
        </div>  
        </div>
        
        
        
        
             
      </section>
      
        
    )

  }
  
}
