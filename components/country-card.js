
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import React from 'react';
export default class CountryCard extends React.Component  {
	constructor(props){
		super(props)		
	  }
	 getCurrencies(){
		var currencyTxt="";
	
	for(var h in this.props.country.currencies){
		if(h<1){
			currencyTxt+=this.props.country.currencies[h].name;
		}else{
			currencyTxt+=","+this.props.country.currencies[h].name;
		}
		return currencyTxt
	}

	}
	
	  getCurrentTime=utcStr=>{
		
		var hour = utcStr[0].slice(4, 6);
		var minute = utcStr[0].slice(7, 9);
		var inputTzOffset=parseInt(hour);
		var now = new Date(); // get the current time

    var currentTzOffset = -now.getTimezoneOffset() / 60 // in hours, i.e. -4 in NY
    var deltaTzOffset = inputTzOffset - currentTzOffset; // timezone diff

    var nowTimestamp = now.getTime(); // get the number of milliseconds since unix epoch 
    var deltaTzOffsetMilli = deltaTzOffset * 1000 * 60 * 60; // convert hours to milliseconds (tzOffsetMilli*1000*60*60)
    var outputDate = new Date(nowTimestamp + deltaTzOffsetMilli) // your new Date object with the timezone offset applied.
    var dd=outputDate.getDate();
    var yyyy=outputDate.getFullYear();
    var month = new Array();
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "Jun";
    month[6] = "Jul";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";
    var MM = month[outputDate.getMonth()];
    

    return dd.toString()+" "+MM+" "+yyyy.toString()+" "+(outputDate.getHours()<10?"0"+outputDate.getHours():outputDate.getHours())+":"+outputDate.getMinutes();
	}
    renderCountryCard=()=>{
		return(
			<div className={`${styles.country_container} container shadow-sm bg-white`}   >
				<div className="row">
					<div className="col-md-4">
						<img src={this.props.country.flag} className="img-fluid"/>
					</div>
					<div className="col-md-8" >
						<div className="row">
							<div className={`${styles.country_with_detail_country_name} col-md-12`}  >
								{this.props.country.name}
							</div>
							
						</div>
						<div className="row" >
							<div className={`${styles.country_detail_container} col-md-12`}>Currency:
							{this.getCurrencies}
							</div>
						</div>
						<div className="row">
							<div className={`${styles.country_detail_container} col-md-12`}>Current date and time: {this.getCurrentTime(this.props.country.timezones)}</div>
							
						</div>
							<div className="row">
								
								<div className={`${styles.button_container} col-md-6`}>
									<Link href={`https://www.google.co.in/maps/place/${this.props.country.name}`}>
										<a>
											<button type="button" className={`${styles.show_button } btn btn-outline-primary btn-block`} >Show Map</button>
										</a>
									</Link>	
								</div>
					
								<div className={`${styles.button_container} col-md-6`}>
									<Link href={`/countries/${this.props.country.alpha3Code}`}>
										
										<a>
										<button type="button" className={`${styles.show_button } btn btn-outline-primary btn-block`}>Show Detail</button>
										</a>
									   </Link>
									
									
								</div>
							</div>
						</div>
					</div>
				</div>
		);

	};
	render(){
		return (	
			this.renderCountryCard()	   
	  )

	}

  
}
