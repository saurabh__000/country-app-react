import React from 'react'
import styles from '../styles/Detail.module.css'

export default class ErrorBanner extends React.Component {
  constructor(props){
    super(props)
  }
  render(){
    return (

        <section>
            <div className="row justify-content-center mt-3" >
                <div className="col-md-6">
                    <h4 className={`${styles.country_name_heading}`}>{this.props.message}</h4>
                </div>
            </div>
        </section>
        )
        }
    }