
  export async function getAllCountries() {
    // Instead of the file system,
    // fetch post data from an external API endpoint
    const res = await fetch('https://restcountries.eu/rest/v2/all')
    console.log(res.json)
    return res.json()
  }